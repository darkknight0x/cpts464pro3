package server;

import java.util.HashMap;

public class Main {
	private static HashMap<String, Node> nodes;
	private static Node self;
	private static void loadServers() {
		nodes = new HashMap<String, Node>();
		/*nodes.put("Caesar", new Node(false, "Caesar", "localhost", 51060, 0));
		nodes.put("Brutus", new Node(false, "Brutus", "localhost", 51061, 0));
		nodes.put("Pompus" ,new Node(false, "Pompus", "localhost", 51062, 0));
		nodes.put("Operachorus", new Node(false, "Operachorus", "localhost", 51063, 0));
		nodes.put("Overanxious", new Node(false, "Overanxious", "localhost", 51064, 0));*/
		
		
		//TODO: update the urls to the correct urls for project3
		nodes.put("Caesar", new Node(false, "Caesar", "Caeser.cpt464ac-project3.CPTS464.isi.deterlab.net", 51060, 0));
		nodes.put("Brutus", new Node(false, "Brutus", "Brutus.cpt464ac-project3.CPTS464.isi.deterlab.net", 51061, 0));
		nodes.put("Pompus" ,new Node(false, "Pompus", "Pompus.cpt464ac-project3.CPTS464.isi.deterlab.net", 51062, 0));
		nodes.put("Operachorus", new Node(false, "Operachorus", "Operachorus.cpt464ac-project3.CPTS464.isi.deterlab.net", 51063, 0));
		nodes.put("Overanxious", new Node(false, "Overanxious", "Overanxious.cpt464ac-project3.CPTS464.isi.deterlab.net", 51064, 0));
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		loadServers();
		//see if we are in the list of known nodes
		if(!nodes.containsKey(args[0])) {
			System.out.println("Unknown host Name");
			return;
		}
		self = nodes.get(args[0]);
		Server serv = new Server(self, nodes);
		Thread server = new Thread(serv);
		server.start();
		
		for(String unit : self.listDir()) {
			CommandParser command = new CommandParser(self.getName(),
									self.getIncrementedClock(),
									"iHave",
									unit,
									null);
			nodes.get("Overanxious").send(command);
		}
		
		//do user interface stuff here...
		StartMenu menu = new StartMenu(self, nodes);
		menu.start();
		
	}

}
