package server;
import java.util.StringTokenizer;
/**
 * A class to support parameterized requests between nodes.
 * @author Joseph Carter
 *
 */
public class CommandParser {

	String commandString;
	String command;
	String from;
	String clock;
	String unitName;
	String location;

	/**
	 * Takes a comma separated command string and constructs 
	 * @param command, a command string to be parsed.
	 */
	public CommandParser(String command) {
		this.commandString = new String(command);
		parse();
	}
	
	/**
	 * Constructs a command string composed of the following values. 
	 * @param self, the name of the node sending the command.
	 * @param clock, the current value of the logical clock.
	 * @param cmd, the command to issue.
	 * @param unitName, The name of the unit.
	 * @param location, depending on the command, this is either
	 * the name of the node that has the unit, or the contents of the
	 * file.
	 */
	public CommandParser(String self, int clock, String cmd,
						 String unitName, String location) {
		command = new String(cmd);
		from = new String(self);
		this.clock = Integer.toString(clock);
		this.unitName = unitName;
		this.location = location;
	}
	
	/**
	 * Copy constructor
	 * @param cmd, the command object to copy
	 */
	public CommandParser(CommandParser cmd) {
		this.commandString = new String(cmd.commandString);
		this.command = new String(cmd.command);
		this.from = new String(cmd.from);
		this.clock = new String(cmd.clock);
		if(unitName == null) {
			this.unitName = " ";
		} else {
			this.unitName = new String(cmd.unitName);
		}
		if(location == null) {
			this.location = " ";
		} else {
			this.location = new String(cmd.location);
		}
	}
	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return new String(from+","+clock+","+command+","+unitName+","+location+",");						  
	}
	
	/**
	 * returns the bytes of the command string.
	 * @return
	 */
	public byte [] getBytes() {
		return this.toString().getBytes();
	}
	
	/**
	 * extracts the values form the command string
	 */
	private void parse() {
		StringTokenizer token = new StringTokenizer(commandString, ",", false);
		from = token.nextToken();
		clock = token.nextToken();
		command = token.nextToken();
		this.unitName = token.nextToken();
		this.location = token.nextToken();
	}
	
	/**
	 * Returns the command to be performed by the node
	 * @return, the command
	 */
	public String getCommand() {
		return command;
	}
	
	/**
	 * Returns the name of the node that issued the command.
	 * @return name of the node that issued the command
	 */
	public String getSender() {
		return from;
	}
	
	/**
	 * Returns the value of the sending node's local clock at the time of
	 * issuing the command.
	 * @return
	 */
	public int getClock() {
		return Integer.parseInt(clock);
	}
	
	public String getUnitName() {
		return unitName;
	}
	
	public String getUnitLocation() {
		return location;
	}
	
	public String getFileContent() {
		return location;
	}

}
