/**
 * 
 */
package server;

import java.net.*;
import java.util.HashMap;
/**
 * @author joe
 *
 */
public class Worker implements Runnable {
	DatagramSocket socket = null;
	Node self;
	CommandParser cmd;
	HashMap<String, Node> nodes;
	HashMap<String, String> unitIndexes;

	public Worker(DatagramSocket socket, Node self, CommandParser cmd, HashMap<String, Node> nodes, HashMap<String, String> unitIndexes) {
        this.socket = socket;
        this.self = self;
        this.cmd = cmd;
        this.nodes = nodes;
		this.unitIndexes = unitIndexes;
    }
	
	public void run() {
		Node node = nodes.get(cmd.getSender());
		
		self.incrementClock();
		synchronized(node) {
			if(node != self) {
				node.setClock(cmd.getClock());
			}
		}
		
		if(cmd.getCommand().equals("giveMe")) {
			CommandParser reply = null;
			try {
				reply = new CommandParser(self.getName(),
										  self.getIncrementedClock(),
										  "take",
										  cmd.getUnitName(),
										  " ");
				self.delFile(cmd.getUnitName());
			}catch(Exception e){
				reply = new CommandParser(self.getName(),
						  self.getIncrementedClock(),
						  "unknownUnit",
						  cmd.getUnitName(),
						  " ");
			}
			node.send(reply);
		} else if(cmd.getCommand().equals("take")) {
			self.mkFile(cmd.getUnitName());			
			//tell overanxious we have the unit
			CommandParser command = new CommandParser(self.getName(),
													self.getIncrementedClock(),
													"iHave",
													cmd.getUnitName(),
													null);
			nodes.get("Overanxious").send(command);
		} else if(cmd.getCommand().equals("unitLocation")) {
			//overanxious is telling us where a unit is
			System.out.println(cmd.getUnitLocation()+" has unit: "+cmd.getUnitName());
		} else if(cmd.getCommand().equals("unknownUnit")) {
			System.out.println("unit: "+cmd.getUnitName()+" not found.");
		} else if (cmd.getCommand().equals("iHave")) {
			unitIndexes.put(cmd.getUnitName(), cmd.getSender());
		} else if (cmd.getCommand().equals("findUnit")) {
			CommandParser command = null;
			if(!unitIndexes.containsKey(cmd.getUnitName())) {
				command = new CommandParser(self.getName(),
						self.getIncrementedClock(),
						"unknownUnit",
						cmd.getUnitName(),
						null);
			} else {
				String location = unitIndexes.get(cmd.getUnitName());
				command = new CommandParser(self.getName(),
						  					self.getIncrementedClock(),
						  					"unitLocation",
						  					cmd.getUnitName(),
						  					location);
			}
			node.send(command);
		} else {
			//bad command?
			//maybe we should log this?	
			System.out.println("Unknown command: " +cmd.getCommand()+" from "+cmd.getSender());
			return;
		}
	
	}
	
}
