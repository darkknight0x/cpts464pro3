

package server;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.HashMap;

public class Overanxious {
	private Node self;
	private String myName;
	private HashMap<String, Node> nodes;
	private HashMap<String, String> unitIndexes;
	private DatagramSocket aSocket;

	/**
	 * @param name	name of the node
	 */
		public static void main(String[] args) {
		Overanxious over = new Overanxious("Overanxious");
		over.run();
	}
		public void run() {
		try {
			aSocket = new DatagramSocket(self.getPort());
		while(true) {
			byte[] buffer = new byte[4096];
			
			DatagramPacket request = new DatagramPacket(buffer, buffer.length);
			try {
				aSocket.receive(request);
			} catch (IOException e) {
				e.printStackTrace();
			}
			String foo = null;
			try {
				foo = new String(request.getData(), "UTF-8");
			} catch (UnsupportedEncodingException e) {	}
			Worker worker = new Worker(aSocket, self,
									   new CommandParser(foo), nodes, unitIndexes);
			
			Thread tmp = new Thread(worker);
			tmp.start();
		}
		} catch (SocketException e) {
			e.printStackTrace();
		}
		}
	public Overanxious(String name) {
		loadServers();
		myName = new String(name);
		unitIndexes = new HashMap<String, String>();
		
		//see if we are in the list of known nodes
		if(!nodes.containsKey(myName)) {
			System.out.println("Unknown host Name");
			return;
		}
		self = nodes.get(myName);
	}
	private void loadServers() {
		nodes = new HashMap<String, Node>();
		/*nodes.put("Caesar", new Node(false, "Caesar", "localhost", 51060, 0));
		nodes.put("Brutus", new Node(false, "Brutus", "localhost", 51061, 0));
		nodes.put("Pompus" ,new Node(false, "Pompus", "localhost", 51062, 0));
		nodes.put("Operachorus", new Node(false, "Operachorus", "localhost", 51063, 0));
		nodes.put("Overanxious", new Node(false, "Overanxious", "localhost", 51064, 0));*/
		
		nodes.put("Caesar", new Node(false, "Caesar", "Caeser.cpt464ac-project3.CPTS464.isi.deterlab.net", 51060, 0));
		nodes.put("Brutus", new Node(false, "Brutus", "Brutus.cpt464ac-project3.CPTS464.isi.deterlab.net", 51061, 0));
		nodes.put("Pompus" ,new Node(false, "Pompus", "Pompus.cpt464ac-project3.CPTS464.isi.deterlab.net", 51062, 0));
		nodes.put("Operachorus", new Node(false, "Operachorus", "Operachorus.cpt464ac-project3.CPTS464.isi.deterlab.net", 51063, 0));
		nodes.put("Overanxious", new Node(false, "Overanxious", "Overanxious.cpt464ac-project3.CPTS464.isi.deterlab.net", 51064, 0));
	}
}