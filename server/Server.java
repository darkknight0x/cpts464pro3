/**
 * 
 */
package server;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.HashMap;

/**
 * @author Joseph Carter
 *
 */
public class Server implements Runnable {
	private Node self;
	private HashMap<String, Node> nodes;
	private DatagramSocket aSocket;
	
	/**
	 * @param name	name of the node
	 * @param nodes2 
	 */
	public Server(Node self, HashMap<String, Node> nodes) {
		this.nodes = nodes;
		this.self = self;
	}

	
	/**
	 * server loop, waits for a requests and processes it
	 */
	public void run() {
		try {
			aSocket = new DatagramSocket(self.getPort());
		while(true) {
			byte[] buffer = new byte[4096];
			
			DatagramPacket request = new DatagramPacket(buffer, buffer.length);
			try {
				aSocket.receive(request);
			} catch (IOException e) {
				e.printStackTrace();
			}
			String foo = null;
			try {
				foo = new String(request.getData(), "UTF-8");
			} catch (UnsupportedEncodingException e) {	}
			Worker worker = new Worker(aSocket, self,
									   new CommandParser(foo), nodes, null);
			
			Thread tmp = new Thread(worker);
			tmp.start();
		}
		} catch (SocketException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Loads the information about all the servers, if I were going to use this
	 * software more, I would have this load form an XML file or a database of
	 * some sort. I would also probably add a command for node discovery. 
	 */
	
	public void printVectorClock() {
		System.out.print("<"+nodes.get("Caesar").getClock()+", "+
							nodes.get("Brutus").getClock()+", "+
							nodes.get("Pompus").getClock()+", "+
							nodes.get("Operachorus").getClock()+"> ");
		
	}
}


