/**
 * 
 */
package server;

import java.net.*;
import java.io.*;
/**
 * @author joe
 *
 */
public class ExitWorker implements Runnable{
	
	private Node self;
	private Node node;
	
	public ExitWorker(Node self, Node node) {
		this.self = self;
		this.node = node;
	}
	
	public void run() {
		Socket clientSocket = null;
		DataOutputStream outToServer = null;
		CommandParser command = new CommandParser(self.getName(),
												  self.getIncrementedClock(),
												  "Exit",
												  " ", " ");
		try {
			clientSocket = new Socket(node.getAddress(), node.getPort());
			outToServer = new DataOutputStream(clientSocket.getOutputStream());
			
			if(node.isRecording) {
				System.out.print(command.getClock()+": ");
				node.saveChannelOut(command);
			}
			outToServer.writeBytes(command.toString()+ "\n");	
			clientSocket.close();
			
		} catch (IOException e1) {
			e1.printStackTrace();
		} finally {
			try {
				clientSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}		
}
