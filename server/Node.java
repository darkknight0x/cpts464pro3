package server;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

public class Node {
	private int clock;
	private String name;
	private String url;
	private int port;
	private String[] fileList ;
	String dirpath = "";
	public volatile Boolean isRecording;
	public volatile Boolean stateSaved;
	List<CommandParser> channelIn;
	List<CommandParser> channelOut;
	
	/**
	 * @param armySize	The initial size of the nodes army
	 * @param name	the name of the node
	 * @param address	the url of the node
	 * @param port	the port number on which the node is listening
	 * @param clk	the initial value of the nodes clock
	 */
	public Node(boolean reported, String name, String address, int port, int clk) {
		this.name = name;
		this.url = address;
		this.clock = clk;
		this.port = port;
		channelIn = new ArrayList<CommandParser>();
		channelOut = new ArrayList<CommandParser>();
		isRecording = false;
		//isRecording = true;
		stateSaved = false;
		fileList = null;
		dirpath = "";
	}
	

	public synchronized void saveChannelIn(CommandParser cmd) {
		channelIn.add(new CommandParser(cmd));
		System.out.println("IN: Node "+name+": "+cmd.toString());
	}
	
	public synchronized void saveChannelOut(CommandParser cmd) {
		channelOut.add(cmd);
		System.out.println("OUT: Node "+name+": "+cmd.toString());
	}
	
	
	/**
	 * @return value of the nodes logical clock
	 */
	public synchronized int getClock() {
		return clock;
	}
	
	
	/**
	 * Sets the nodes logical clock to clk 
	 * @param clk the value of the logical clock
	 */
	public synchronized void setClock(int clk) {
		this.clock = clk;
	}
	
	/**
	 * Increments the logical clock if the node by 1
	 */
	public synchronized void incrementClock() {
		clock++;
	}
	
	
	/**
	 * Increments the clock and returns the new value
	 * @return Value of the clock
	 */
	public synchronized int getIncrementedClock() {
		incrementClock();
		return getClock();
	}
	
	public String getName() {
		return name;
	}
	
	
	/**
	 * @return url of the server
	 */
	public String getAddress() {
		return url;
	}
	
	
	/**
	 * @return port number of the server
	 */
	public int getPort() {
		return port;
	}
	
	public void send(CommandParser command) {
		DatagramSocket socket = null;
		try {
			socket = new DatagramSocket();
		} catch (SocketException e1) {
			e1.printStackTrace();
		}
		byte [] reply = command.getBytes();
		
		InetAddress reciever = null;
		//get address to reply too.
		try {
			reciever = InetAddress.getByName(getAddress());
		} catch (UnknownHostException e) {
			return;
		}

        DatagramPacket response = new DatagramPacket(reply, reply.length,
        		reciever, getPort());
        try {
			socket.send(response);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/*
	 * Create a new empty file to be written to
	 */
	public void mkFile(String filename){
		String filepath = dirpath +"/"+filename;
		
		
		File file = new File(filepath);
		 String text = "";
		 if(!file.exists()){
	        Writer writer;
			try {
				writer = new BufferedWriter(new FileWriter(file));
				writer.write(text);
		        writer.close();
			} catch (IOException e) {
				System.out.println("Writing File Error");
				e.printStackTrace();
			}
		 }
		 else{
			 System.out.println("Cannnot write file "+filepath+ " It already exisit!");
		 }
	}
	/**
	 * Deletes a file from the node/Gen Current dir
	 * @throws Exception 
	 */

	public void delFile(String filename) throws Exception {
		String filepath = dirpath + "/" + filename;
		File file = new File(filepath);

		if (file.delete()) {
			System.out.println(file.getName() + " was deleted!");
		} else {
			System.out.println("Delete operation is failed.");
			throw new Exception();
		}
	}
	/**
	 * Returns List The contents of the directory
	 */
	
	public String[] listDir(){
		String path = null;
		try {
			path = new File(".").getCanonicalPath().toString();
		} catch (IOException e) {
			System.out.println("General Directory Does not exisit");
			e.printStackTrace();
		}
		
		
		if(name == "Brutus"){
			dirpath = path+ "/Brutus";//if in root
			//dirpath = path+ "/server/Caesar";//if in root/server
		}
		else if (name == "Caesar"){
			dirpath = path+ "/Caesar";//if in root
			//dirpath = path+ "/server/Caesar";//if in root/server
		}
		else if (name == "Pompus"){
			dirpath = path+ "/Pompus";//if in root
			//dirpath = path+ "/server/Caesar";//if in root/server
		}
		else if (name == "Operachorus"){
			dirpath = path+ "/Operachorus";//if in root
			//dirpath = path+ "/server/Caesar";//if in root/server
		}
		File f = new File(dirpath);
		if(f.isDirectory()){
			fileList = f.list();
		}

		return fileList;
	}
	
}
