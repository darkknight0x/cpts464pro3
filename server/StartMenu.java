
package server;

import java.io.File;
//import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;


public class StartMenu {
	Node self;
	HashMap<String, Node> nodes;
/*	public static void main(String[] args) {
		StartMenu sm = new StartMenu();
		sm.start();
	}*/
	public StartMenu(Node anode,HashMap<String, Node> nodesList ){
		self = anode;
		nodes = nodesList;
	}
	public void start(){
		boolean runMenu = true;
		int selection;
		Scanner usrInput = new Scanner(System.in);
		while(runMenu){
			System.out.println("1. List");
			System.out.println("2. Search");
			System.out.println("3. Request");
			System.out.println("0. Quit");
			System.out.println("General Choice: "); 
	        try{
	        	selection = Integer.parseInt(usrInput.next());
	        
		        switch (selection) {
		         case 1: list();
		         		break;
		         case 2: search(); 
		         		break;
		         case 3: request(); 
		         		break;
		         case 0:
		         		System.out.println("\n\n\n\n\nExiting...."); 
		         		System.exit(0); 
		         		break;
		         default: 
	                    System.out.println("\nInvalid choice.\n");
	                    break;
		         }//End Switch
	        }//end tryBlock
	        catch(NumberFormatException ex){
	        	System.out.println("\nInvalid Choice\n");
	        }
	        
		}//End WHile
		
	}//ENd start
	///**************************************
	//			TEMP functions for choices
	//************************************
	public void list(){
		String[] fileList =	self.listDir();
		
		for(int i=0; i<fileList.length;i++){
			System.out.println(fileList[i]);
		}

	}

	public void search(){	
		Scanner usrInput = new Scanner(System.in);
		System.out.println("\nWhat is the tag of the soldier you are looking for?");
		String selection = usrInput.nextLine().trim();
		
		CommandParser command = new CommandParser(self.getName(),
										  self.getIncrementedClock(),
										  "findUnit",
										  selection,
										  "");
		//ask the node for the file
		nodes.get("Overanxious").send(command);	
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	public void request(){
		Scanner usrInput = new Scanner(System.in);
		System.out.println("\nWhat is the name of node that contains the desired unit?");
		String nodeName = usrInput.nextLine().trim();
		System.out.println("\nWhat is the name of the unit to request?");
		String unitName = usrInput.nextLine().trim();
		CommandParser command = new CommandParser(self.getName(),
										  self.getIncrementedClock(),
										  "giveMe",
										  unitName,
										  null);
		if(nodeName != self.getName())
		{	
			try
			{
			nodes.get(nodeName).send(command);	
			Thread.sleep(5000);
			}
			catch(Exception ex)
			{
				System.out.println("Error sending command to node");
			}
		}
		else
		{
			System.out.println("\nError cannot send file to yourself.");
			
		}
		
	}
}//End StartMenu


